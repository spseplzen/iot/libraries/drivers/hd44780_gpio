#include <stdlib.h>
#include <stdio.h>

#include "hd44780_gpio.h"

/*
void gpio_write(uint8_t pin, uint8_t value) {
    // zde doplnte o vlastni funkci pro ovladani pinu
}

void delay_us(uint32_t us) {
    // zde doplnte o vlastni funkci pro cekani us
}
*/

int main_example ( void )
{
    // Inicializace

    // Nutne nastavit piny pro komunikaci s displejem jako vystupni

    
    hd44780_t mylcd;
    
    // hd44780_gpio_write_register(&mylcd, gpio_write);
    // hd44780_delay_us_register(&mylcd, delay_us);
    
    hd44780_config_t config;
    config.function_set.data_mode = HD44780_FUNCTION_SET_DATA_MODE_4b;
    config.function_set.font_size = HD44780_FUNCTION_SET_FONT_SIZE_5X8;
    config.function_set.line_mode = HD44780_FUNCTION_SET_LINE_MODE_2LINE;
    
    config.display_control.power    = HD44780_CONTROL_POWER_ON;
    config.display_control.cursor   = HD44780_CONTROL_CURSOR_OFF;
    config.display_control.blinking = HD44780_CONTROL_BLINKING_OFF;
    
    config.entry_mode.increment = HD44780_ENTRY_MODE_INCREMENT_SOFTINC;
    config.entry_mode.shift     = HD44780_ENTRY_MODE_SHIFT_NOSH;
    
    config.dimensions.x = HD44780_DIMENSION_X;
    config.dimensions.y = HD44780_DIMENSION_Y;
    
    hd44780_pins_t pins;
    pins.data.d4 = 0;
    pins.data.d5 = 1;
    pins.data.d6 = 2;
    pins.data.d7 = 3;
    pins.comm.en = 4;
    pins.comm.rs = 5;
    
    hd44780_init(&mylcd, &config, &pins, 0, 0);
    
    hd44780_clear(&mylcd);

    hd44780_set_cursor(&mylcd, 0, 0);
    hd44780_write_string(&mylcd, "Miluju IoT :-)\r\n");
    hd44780_write_string(&mylcd, "Cislo: ");

    while ( true )
    {
        
        char buff[16];
        static int16_t cislo = -99;
    
        hd44780_set_cursor(&mylcd, 7, 1);
        
        sprintf(buff, "%+-3d", cislo++);
        
        hd44780_write_string(&mylcd, buff);
        
        if(cislo > 99) cislo = -99;
        
        delay_us(100000);
        
    }

    return (EXIT_FAILURE);
}
