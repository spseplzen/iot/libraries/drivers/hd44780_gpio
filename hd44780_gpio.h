/*
 * File:   hd44780.h
 * Author: Miroslav Soukup
 * Description: Header file of hd44780 LCD display driver.
 * 
 */


#ifndef HD44780_H
#define HD44780_H

#include <stdint.h>
#include <stdbool.h>


// DOM-IGNORE-BEGIN
#ifdef __cplusplus  // Provide C++ Compatibility
    extern "C" {
#endif
// DOM-IGNORE-END

        
#define HD44780_DIMENSION_X UINT8_C(16)
#define HD44780_DIMENSION_Y UINT8_C(2)

#define HD44780_INSTRUCTION_CLEAR_DISPLAY               UINT8_C(0b00000001)
#define HD44780_INSTRUCTION_RETURN_HOME                 UINT8_C(0b00000010)
#define HD44780_INSTRUCTION_ENTRY_MODE_SET              UINT8_C(0b00000100)
#define HD44780_INSTRUCTION_DISPLAY_ON_OFF_CONTROL      UINT8_C(0b00001000)
#define HD44780_INSTRUCTION_CURSOR_OR_DISPLAY_SHIFT     UINT8_C(0b00010000)
#define HD44780_INSTRUCTION_FUNCTION_SET                UINT8_C(0b00100000)
#define HD44780_INSTRUCTION_SET_CGRAM                   UINT8_C(0b01000000)
#define HD44780_INSTRUCTION_SET_DDRAM                   UINT8_C(0b10000000)
        


typedef struct hd44780_descriptor_t hd44780_t;
typedef struct hd44780_config_descriptor_t hd44780_config_t;
typedef struct hd44780_pins_descriptor_t hd44780_pins_t;
typedef struct hd44780_data_pins_descriptor_t hd44780_data_pins_t;
typedef struct hd44780_comm_pins_descriptor_t hd44780_comm_pins_t;
typedef struct hd44780_position_descriptor_t hd44780_position_t;
typedef struct hd44780_entry_mode_descriptor_t hd44780_entry_mode_t;
typedef struct hd44780_function_set_descriptor_t hd44780_function_set_t;
typedef struct hd44780_control_descriptor_t hd44780_control_t;

typedef void (*hd44780_gpio_write_funcptr_t)(uint8_t pin, uint8_t value);
typedef void (*hd44780_delay_us_funcptr_t)(uint32_t us);



typedef enum{
    HD44780_ENTRY_MODE_INCREMENT_INC     = 0,
    HD44780_ENTRY_MODE_INCREMENT_SOFTINC = 1,
    HD44780_ENTRY_MODE_INCREMENT_NOINC   = 2
} HD44780_ENTRY_MODE_INCREMENT_e;

typedef enum{
    HD44780_ENTRY_MODE_SHIFT_SH   = 0,
    HD44780_ENTRY_MODE_SHIFT_NOSH = 1
} HD44780_ENTRY_MODE_SHIFT_e;

typedef enum{
    HD44780_CONTROL_POWER_OFF = 0,
    HD44780_CONTROL_POWER_ON  = 1
} HD44780_CONTROL_POWER_e;

typedef enum{
    HD44780_CONTROL_CURSOR_OFF = 0,
    HD44780_CONTROL_CURSOR_ON  = 1
} HD44780_CONTROL_CURSOR_e;

typedef enum{
    HD44780_CONTROL_BLINKING_OFF = 0,
    HD44780_CONTROL_BLINKING_ON  = 1
} HD44780_CONTROL_BLINKING_e;

typedef enum{
    HD44780_FUNCTION_SET_DATA_MODE_8b = 0,
    HD44780_FUNCTION_SET_DATA_MODE_4b = 1
} HD44780_FUNCTION_SET_DATA_MODE_e;

typedef enum{
    HD44780_FUNCTION_SET_LINE_MODE_1LINE = 0,
    HD44780_FUNCTION_SET_LINE_MODE_2LINE = 1
} HD44780_FUNCTION_SET_LINE_MODE_e;

typedef enum{
    HD44780_FUNCTION_SET_FONT_SIZE_5X8  = 0,
    HD44780_FUNCTION_SET_FONT_SIZE_5X10 = 1
} HD44780_FUNCTION_SET_FONT_SIZE_e;

struct hd44780_position_descriptor_t{
    volatile uint8_t x;
    volatile uint8_t y;
};

struct hd44780_entry_mode_descriptor_t{
    HD44780_ENTRY_MODE_INCREMENT_e increment;
    HD44780_ENTRY_MODE_SHIFT_e shift;
};

struct hd44780_function_set_descriptor_t{
    HD44780_FUNCTION_SET_DATA_MODE_e data_mode;
    HD44780_FUNCTION_SET_LINE_MODE_e line_mode;
    HD44780_FUNCTION_SET_FONT_SIZE_e font_size;
};

struct hd44780_control_descriptor_t{
    HD44780_CONTROL_POWER_e power;
    HD44780_CONTROL_CURSOR_e cursor;
    HD44780_CONTROL_BLINKING_e blinking;
};

struct hd44780_config_descriptor_t{
    hd44780_function_set_t function_set; // 8/4 bit communication
    hd44780_entry_mode_t entry_mode;     // increment, shift
    hd44780_control_t display_control;   // display on/off, cursor on/off, blinking on/off
    hd44780_position_t dimensions;       // dimensions (x,y)
};

struct hd44780_data_pins_descriptor_t{
    uint16_t d0;
    uint16_t d1;
    uint16_t d2;
    uint16_t d3;
    uint16_t d4;
    uint16_t d5;
    uint16_t d6;
    uint16_t d7;
};

struct hd44780_comm_pins_descriptor_t{
    uint16_t en;
    uint16_t rs;
};

struct hd44780_pins_descriptor_t{
    hd44780_data_pins_t data;
    hd44780_comm_pins_t comm;
};

struct hd44780_descriptor_t{
    hd44780_config_t config;
    hd44780_pins_t pins;
    hd44780_position_t pos;
    hd44780_gpio_write_funcptr_t gpio_write;
    hd44780_delay_us_funcptr_t delay_us;
};
        


uint8_t hd44780_set_cursor(hd44780_t *me, uint8_t x, uint8_t y);

uint8_t hd44780_get_cursor(const hd44780_t *me, uint8_t *x, uint8_t *y);

uint8_t hd44780_write_char(hd44780_t *me, const char c);

uint8_t hd44780_write_char_xy(hd44780_t *me, const char c, const uint8_t x, const uint8_t y);

uint8_t hd44780_write_string(hd44780_t *me, const char *s);

uint8_t hd44780_clear_xy(hd44780_t *me, const uint8_t x, const uint8_t y);

uint8_t hd44780_clear_row(hd44780_t *me, const uint8_t y);

uint8_t hd44780_clear(hd44780_t *me);

uint8_t hd44780_init (hd44780_t *me, const hd44780_config_t *cfg, const hd44780_pins_t *pins, const uint8_t init_x, const uint8_t init_y);

uint8_t hd44780_gpio_write_register (hd44780_t *me, hd44780_gpio_write_funcptr_t gpio_write_funcptr);

uint8_t hd44780_delay_us_register (hd44780_t *me, hd44780_delay_us_funcptr_t delay_us_funcptr);



// DOM-IGNORE-BEGIN
#ifdef __cplusplus  // Provide C++ Compatibility

}

#endif
// DOM-IGNORE-END

#endif // HD44780_H
