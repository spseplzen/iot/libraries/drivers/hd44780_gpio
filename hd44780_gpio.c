/*
 * File:   hd44780.c
 * Author: Miroslav Soukup
 * Description: Source file of hd44780 LCD display driver.
 * 
 */

#include "hd44780_gpio.h"

#include <stdint.h>
#include <stdbool.h>
#include <ctype.h>



static inline void _hd44780_wait_busy(const hd44780_t *me) {
    me->delay_us(1000);
}

static inline void _hd44780_enable_toggle(const hd44780_t *me) {
    me->gpio_write(me->pins.comm.en, 1); // set enable
    me->delay_us(1);
    me->gpio_write(me->pins.comm.en, 0); // set enable
    me->delay_us(1);
}

static void _hd44780_write(const hd44780_t *me, uint8_t data, uint8_t rs) {
    
    me->gpio_write(me->pins.comm.rs, rs); // set rs pin via rs value
    
    if (me->config.function_set.data_mode == HD44780_FUNCTION_SET_DATA_MODE_4b) {
        
        me->gpio_write(me->pins.data.d7, (data >> 7) & 1);
        me->gpio_write(me->pins.data.d6, (data >> 6) & 1);
        me->gpio_write(me->pins.data.d5, (data >> 5) & 1);
        me->gpio_write(me->pins.data.d4, (data >> 4) & 1);
        me->delay_us(1);
        _hd44780_enable_toggle(me);
        
        
        me->gpio_write(me->pins.data.d7, (data >> 3) & 1);
        me->gpio_write(me->pins.data.d6, (data >> 2) & 1);
        me->gpio_write(me->pins.data.d5, (data >> 1) & 1);
        me->gpio_write(me->pins.data.d4, (data >> 0) & 1);
        me->delay_us(1);
        _hd44780_enable_toggle(me);

        me->gpio_write(me->pins.data.d4, 1);
        me->gpio_write(me->pins.data.d5, 1);
        me->gpio_write(me->pins.data.d6, 1);
        me->gpio_write(me->pins.data.d7, 1);
    }
    else if (me->config.function_set.data_mode == HD44780_FUNCTION_SET_DATA_MODE_8b) {
        me->gpio_write(me->pins.data.d0, (data >> 0) & 1);
        me->gpio_write(me->pins.data.d1, (data >> 1) & 1);
        me->gpio_write(me->pins.data.d2, (data >> 2) & 1);
        me->gpio_write(me->pins.data.d3, (data >> 3) & 1);
        me->gpio_write(me->pins.data.d4, (data >> 4) & 1);
        me->gpio_write(me->pins.data.d5, (data >> 5) & 1);
        me->gpio_write(me->pins.data.d6, (data >> 6) & 1);
        me->gpio_write(me->pins.data.d7, (data >> 7) & 1);
        me->delay_us(1);
        _hd44780_enable_toggle(me);
        
        me->gpio_write(me->pins.data.d0, 1);
        me->gpio_write(me->pins.data.d1, 1);
        me->gpio_write(me->pins.data.d2, 1);
        me->gpio_write(me->pins.data.d3, 1);
        me->gpio_write(me->pins.data.d4, 1);
        me->gpio_write(me->pins.data.d5, 1);
        me->gpio_write(me->pins.data.d6, 1);
        me->gpio_write(me->pins.data.d7, 1);
    }
    
    me->delay_us(110);   
}

static void _hd44780_write_command(const hd44780_t *me, uint8_t cmd) {
    _hd44780_wait_busy(me);
    _hd44780_write(me, cmd, 0);
}

static void _hd44780_write_data(const hd44780_t *me, uint8_t data) {
    _hd44780_wait_busy(me);
    _hd44780_write(me, data, 1);
}

static inline void _hd44780_write_cursor(const hd44780_t *me){
    _hd44780_write_command(me, HD44780_INSTRUCTION_SET_DDRAM | ((me->pos.y) << 6) | (me->pos.x));
}

static inline void _hd44780_increment_cursor(hd44780_t *me){
    if((me->pos.x + 1) >= me->config.dimensions.x){
        me->pos.x = 0;
        if((me->pos.y + 1) >= me->config.dimensions.y) me->pos.y = 0;
        else ++me->pos.y;
    }
    else ++me->pos.x;
}

uint8_t hd44780_set_cursor(hd44780_t *me, uint8_t x, uint8_t y){
    if(((x + 1) >= me->config.dimensions.x) || ((y + 1) > me->config.dimensions.y)) return false;
    
    me->pos.x = x;
    me->pos.y = y;
    
    if(me->config.entry_mode.increment == HD44780_ENTRY_MODE_INCREMENT_INC) _hd44780_write_cursor(me);
    
    return 1;
}

uint8_t hd44780_get_cursor(const hd44780_t *me, uint8_t *x, uint8_t *y){
    *x = me->pos.x;
    *y = me->pos.y;
    return 1;
}

uint8_t hd44780_write_char(hd44780_t *me, const char c){
    if(!isprint(c) && c != '\r' && c != '\n') return 0;
    
    if(c == '\r') me->pos.x = 0;
    if(c == '\n') me->pos.y = (me->pos.y + 1) % me->config.dimensions.y;
    
    if(     me->config.entry_mode.increment == HD44780_ENTRY_MODE_INCREMENT_SOFTINC
        ||  me->config.entry_mode.increment == HD44780_ENTRY_MODE_INCREMENT_NOINC ) _hd44780_write_cursor(me);
    
    if(c != '\r' && c != '\n'){
        _hd44780_write_data(me, (uint8_t) c);
        if(me->config.entry_mode.increment == HD44780_ENTRY_MODE_INCREMENT_SOFTINC) _hd44780_increment_cursor(me);
    }
    
    return 1;
}

uint8_t hd44780_write_char_xy(hd44780_t *me, const char c, const uint8_t x, const uint8_t y){
    if(!isprint(c)) return 0;
    
    if(!hd44780_set_cursor(me, x, y)) return 0;
    if(!hd44780_write_char(me, c)) return 0;
    
    return 1;
}

uint8_t hd44780_write_string(hd44780_t *me, const char *s){
    
    uint8_t _state = 1;
    
    while(*s){
        if(!hd44780_write_char(me, *s)){
            _state = 0;
        }
        ++s;
    }
    
    return _state;
}

uint8_t hd44780_clear_xy(hd44780_t *me, const uint8_t x, const uint8_t y){
    if(!hd44780_set_cursor(me, x, y)) return 0;
    if(!hd44780_write_char(me, ' ')) return 0;
    return 1;
}

uint8_t hd44780_clear_row(hd44780_t *me, const uint8_t y){
    uint8_t _state = 1;
    
    if(!hd44780_set_cursor(me, 0, y)) return 0;
    
    for(uint8_t i = 0; i < me->config.dimensions.x; ++i){
        if(!hd44780_write_char(me, ' ')){
            _state = 0;
            break;
        }
    }
    
    return _state;
}

uint8_t hd44780_clear(hd44780_t *me){
    uint8_t _state = 1;
    
    for(uint8_t i = 0; i < me->config.dimensions.y; ++i){
        if(!hd44780_clear_row(me, i)){
            _state = 0;
            break;
        }
    }
    
    return _state;
}

uint8_t hd44780_init (hd44780_t *me, const hd44780_config_t *cfg, const hd44780_pins_t *pins, const uint8_t init_x, const uint8_t init_y) {
    
    me->config.function_set.data_mode = cfg->function_set.data_mode;
    me->config.function_set.line_mode = cfg->function_set.line_mode;
    me->config.function_set.font_size = cfg->function_set.font_size;
    
    me->config.display_control.power    = cfg->display_control.power;
    me->config.display_control.cursor   = cfg->display_control.cursor;
    me->config.display_control.blinking = cfg->display_control.blinking;
    
    me->config.entry_mode.increment = cfg->entry_mode.increment;
    me->config.entry_mode.shift     = cfg->entry_mode.shift;
    
    me->config.dimensions.x = cfg->dimensions.x;
    me->config.dimensions.y = cfg->dimensions.y;
    
    me->pos.x = init_x;
    me->pos.y = init_y;
    
    me->pins.comm.en = pins->comm.en;
    me->pins.comm.rs = pins->comm.rs;
    me->pins.data.d0 = pins->data.d0;
    me->pins.data.d1 = pins->data.d1;
    me->pins.data.d2 = pins->data.d2;
    me->pins.data.d3 = pins->data.d3;
    me->pins.data.d4 = pins->data.d4;
    me->pins.data.d5 = pins->data.d5;
    me->pins.data.d6 = pins->data.d6;
    me->pins.data.d7 = pins->data.d7;
    
    
    /*********************** BEGIN INITIAL STARTUP ***********************/
    me->gpio_write(me->pins.comm.rs, 1); // need to be here for correct power up
    me->gpio_write(me->pins.comm.en, 1);
    me->delay_us(20000); // power up wait (more than 16 ms)
    me->gpio_write(me->pins.comm.rs, 0); // set default values
    me->gpio_write(me->pins.comm.en, 0);
    
    
    // begin initial setup in 8bit mode
    if(me->config.function_set.data_mode == HD44780_FUNCTION_SET_DATA_MODE_8b){
        me->gpio_write(me->pins.data.d3, 0);
        me->gpio_write(me->pins.data.d2, 0);
        me->gpio_write(me->pins.data.d1, 0);
        me->gpio_write(me->pins.data.d0, 0);
    }
    me->gpio_write(me->pins.data.d7, 0);
    me->gpio_write(me->pins.data.d6, 0);
    me->gpio_write(me->pins.data.d5, 1);
    me->gpio_write(me->pins.data.d4, 1);
    me->delay_us(1);
    _hd44780_enable_toggle(me);
    
    me->delay_us(5000);
    
    
    // repeate previous commnad
    me->delay_us(1);
    _hd44780_enable_toggle(me);
    me->delay_us(110);
    
    
    // repeate previous commnad
    me->delay_us(1);
    _hd44780_enable_toggle(me);
    me->delay_us(110);
    
    
    // set 4bit mode
    if(me->config.function_set.data_mode == HD44780_FUNCTION_SET_DATA_MODE_4b){
        me->gpio_write(me->pins.data.d7, 0);
        me->gpio_write(me->pins.data.d6, 0);
        me->gpio_write(me->pins.data.d5, 1);
        me->gpio_write(me->pins.data.d4, 0);
        me->delay_us(1);
        _hd44780_enable_toggle(me);

        me->delay_us(110);
    }
    /*********************************************************************/
    

    uint8_t function_set = 0;
    if(me->config.function_set.data_mode == HD44780_FUNCTION_SET_DATA_MODE_8b)      function_set |= 0b00010000; // setup 8bit communication
    if(me->config.function_set.line_mode == HD44780_FUNCTION_SET_LINE_MODE_2LINE)   function_set |= 0b00001000; // setup 2 lines view
    if(me->config.function_set.font_size == HD44780_FUNCTION_SET_FONT_SIZE_5X10)    function_set |= 0b00000100; // setup font size
    
    uint8_t display_control = 0;
    if(me->config.display_control.power == HD44780_CONTROL_POWER_ON)        display_control |= 0b00000100; // turn on display
    if(me->config.display_control.cursor == HD44780_CONTROL_CURSOR_ON)      display_control |= 0b00000010; // turn on cursor
    if(me->config.display_control.blinking == HD44780_CONTROL_BLINKING_ON)  display_control |= 0b00000001; // turn on blinking
    
    uint8_t entry_mode = 0;
    if(me->config.entry_mode.increment == HD44780_ENTRY_MODE_INCREMENT_INC) entry_mode |= 0b00000010; // turn on increment
    if(me->config.entry_mode.shift == HD44780_ENTRY_MODE_SHIFT_SH)          entry_mode |= 0b00000001; // turn on shift

    _hd44780_write_command(me, HD44780_INSTRUCTION_FUNCTION_SET | (function_set & 0b00011100));
    
    _hd44780_write_command(me, HD44780_INSTRUCTION_DISPLAY_ON_OFF_CONTROL); // turn off display
    me->delay_us(5000);
    
    _hd44780_write_command(me, HD44780_INSTRUCTION_CLEAR_DISPLAY); // clear whole display
    me->delay_us(5000);
    
    _hd44780_write_command(me, HD44780_INSTRUCTION_ENTRY_MODE_SET | (entry_mode & 0b00000011));
    
    _hd44780_write_command(me, HD44780_INSTRUCTION_DISPLAY_ON_OFF_CONTROL | (display_control & 0b00000111));
    
    return true;
}

uint8_t hd44780_gpio_write_register (hd44780_t *me, hd44780_gpio_write_funcptr_t gpio_write_funcptr) {
    if(!gpio_write_funcptr) return 0;
    me->gpio_write = gpio_write_funcptr;
    return 1;
}

uint8_t hd44780_delay_us_register (hd44780_t *me, hd44780_delay_us_funcptr_t delay_us_funcptr) {
    if(!delay_us_funcptr) return 0;
    me->delay_us = delay_us_funcptr;
    return 1;
}
